#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""STOCK TICKER DASHBOARD, by Ehssan
Displays stock data on graphs."""

import dash
from dash import html, dcc
from dash.dependencies import Input, Output, State
import pandas_datareader as pdr
import yfinance as yf
from datetime import datetime, timedelta


nasdaq = pdr.nasdaq_trader.get_nasdaq_symbols()
nasdaq = nasdaq[nasdaq.index.notna()]

app = dash.Dash(__name__)
server = app.server

app.title = 'Stock Dashboard'
app.layout = html.Div([
    html.H1('Stock Ticker Dashboard'),
    html.Div(className='container',
             children=[
                 html.Div(className='symbols',
                          children=[
                              html.Label('Stock Symbols', htmlFor='dropdown'),
                              dcc.Dropdown(
                                  id='dropdown',
                                  options=[{'label': symb + ' ' + str(nasdaq.loc[symb]['Security Name']),
                                            'value': symb} for symb in nasdaq.index.astype(str)],
                                  value=['SPYG'],
                                  multi=True,
                              )]
                          ),

                 html.Div(className='dates',
                          children=[
                              html.Label('Start and End Dates',
                                         htmlFor='date-picker-range'),
                              dcc.DatePickerRange(
                                  id='date-picker-range',
                                  max_date_allowed=datetime.today(),
                                  initial_visible_month=datetime.today() - timedelta(weeks=12),
                                  clearable=True
                              )]
                          ),

                 html.Div(className='update',
                          children=[
                              html.Button(
                                  id='update-button',
                                  children='Update',
                                  n_clicks=0,
                              )]),

                 html.Div(className='graphs',
                          children=[
                              dcc.Graph(
                                  id='plot'
                              )]
                          )
             ]
             )])


@ app.callback(Output('plot', 'figure'),
               [State('date-picker-range', 'start_date'),
               State('date-picker-range', 'end_date'),
               State('dropdown', 'value')
                ],
               [Input('update-button', 'n_clicks')])
def update_plot(start, end, selections, click):
    traces = []
    for item in selections:
        data = yf.download(item, start, end)
        traces.append(
            {'x': data.index, 'y': data['Close'].values, 'name': item})
    figure = {
        'data': traces,
        'layout': {'title': ', '.join(selections) + ' Closing Prices'}
    }
    return figure


if __name__ == '__main__':
    app.run_server()
